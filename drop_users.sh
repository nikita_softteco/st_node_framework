#!/bin/sh

export CONTAINER_NAME="auth_mongo"
export DATABASE_NAME="users"

docker exec -i ${CONTAINER_NAME} mongo ${DATABASE_NAME} --eval "db.dropDatabase()"     
