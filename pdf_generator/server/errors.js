class BadRequestError extends Error {
  constructor(message = 'Data validation failed') {
    super(message);
    this.name = 'BadRequest';
    this.status = 400;
  }
}

class UnauthorizedError extends Error {
  constructor(message = 'Invalid credentials') {
    super(message);
    this.name = 'Unauthorized';
    this.status = 401;
  }
}

module.exports = {
  BadRequestError,
  UnauthorizedError,
};
