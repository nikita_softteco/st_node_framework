const fs = require('fs');
const util = require('util');

const schemas = require('../schemas/input_schemas');
const outputSchemas = require('../schemas/output_schemas');
const validate = require('../lib/validation');
const filter = require('../lib/filter_output');

const filesStorage = '/usr/data/app/files';

const asyncReadFile = util.promisify(fs.readFile);

const features = {};

features.get = async function get(query) {
  const request = validate(query, schemas.clients.get.query);

  const { filename } = request;
  const path = `${filesStorage}/clients/${filename}`;

  const fileBuf = await asyncReadFile(path);

  return filter(fileBuf, outputSchemas.clients.get);
};

module.exports = features.get;
