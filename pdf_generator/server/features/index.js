const getClients = require('./clients_get');
const postClients = require('./clients_post');
const getSpec = require('./spec_get');

module.exports = {
  getClients,
  postClients,
  getSpec,
};
