const puppeteer = require('puppeteer');
const pug = require('pug');

const schemas = require('../schemas/input_schemas');
const outputSchemas = require('../schemas/output_schemas');
const validate = require('../lib/validation');
const filter = require('../lib/filter_output');
const { UnauthorizedError } = require('../errors');

const viewsStorage = './templates';
const filesStorage = '/usr/data/app/files';

const clientsForm = pug.compileFile(`${viewsStorage}/clients.pug`);

const generatePDF = async function generate(html, filePath, format = 'A4') {
  const browser = await puppeteer.launch({
    headless: true,
    executablePath: '/usr/bin/chromium-browser',
    args: ['--no-sandbox', '--enable-features=NetworkService'],
  });

  const page = await browser.newPage();

  await page.setContent(html);
  await page.pdf({ path: filePath, format });

  await page.close();
  await browser.close();
};

const features = {};

features.create = async function create(body, query) {
  if (!query.meta) throw new UnauthorizedError('No metadata about current user');

  const request = validate(body, schemas.clients.post.body);
  const { name, clients } = request;

  const form = clientsForm({ name, clients });
  const filename = `${Date.now().toString()}.pdf`;
  const path = `${filesStorage}/clients/${filename}`;

  await generatePDF(form, path);
  return filter({ filename }, outputSchemas.clients.post);
};

module.exports = features.create;
