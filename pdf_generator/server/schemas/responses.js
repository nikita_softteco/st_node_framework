const structure = require('./api_structure');
const outputSchemas = require('./output_schemas');
const { BadRequestError } = require('../errors');
const { cloneStructure, copyDataFieldBetweenSchemas } = require('../lib/operations');

const schemas = cloneStructure(structure);

schemas.clients.get = {
  description: 'Returns pdf by filename',
  errors: [new BadRequestError()],
};

schemas.clients.post = {
  description: 'Creates pdf and returns its filename',
  errors: [new BadRequestError()],
};

const responseSchema = copyDataFieldBetweenSchemas(outputSchemas, schemas);

module.exports = responseSchema;
