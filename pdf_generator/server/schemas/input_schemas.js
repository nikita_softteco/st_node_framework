const joi = require('@hapi/joi');

const structure = require('./api_structure');

const { cloneStructure } = require('../lib/operations');

const schemas = cloneStructure(structure);

schemas.clients.get.query = joi.object().keys({
  filename: joi.string().min(15).max(20).required(),
});

schemas.clients.post.body = joi.object().keys({
  name: joi.string().min(3).max(10).required(),
  clients: joi.array().items(joi.object().keys({
    name: joi.string().min(3).max(10).required(),
    age: joi.number().min(0).max(110).required(),
  })).min(1).required(),
});

module.exports = schemas;
