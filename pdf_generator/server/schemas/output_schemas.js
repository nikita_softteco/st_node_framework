const structure = require('./api_structure');

const { cloneStructure } = require('../lib/operations');

const schemas = cloneStructure(structure);

schemas.clients.post = {
  common: {
    data: {
      filename: '',
    },
  },
};

schemas.clients.get = {
  common: {
    data: Buffer.from(''),
  },
};

module.exports = schemas;
