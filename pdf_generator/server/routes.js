const express = require('express');

const features = require('./features');

const router = express.Router();

router.get('/clients', async (req, res, next) => {
  let response;
  try {
    response = await features.getClients(req.query);
  } catch (err) {
    return next(err);
  }
  res.send(response);
});

router.post('/clients', async (req, res, next) => {
  let response;
  try {
    response = await features.postClients(req.body, req.query);
  } catch (err) {
    return next(err);
  }
  res.json(response);
});

router.get('/spec', async (req, res, next) => {
  let response;
  try {
    response = await features.getSpec();
  } catch (err) {
    return next(err);
  }
  res.json(response);
});

module.exports = router;
