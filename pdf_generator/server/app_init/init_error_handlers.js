const logger = require('./init_logger');

function init(app) {
  app.use((req, res) => res.status(404).json({
    error: {
      message: 'Not found, pdf generator',
    },
  }));

  app.use((err, req, res, next) => {
    logger.error({ ...req, error: err });
    res.status(err.status || 500).json({
      error: {
        message: err.message,
      },
    });
  });
}

module.exports = init;
