const { createLogger, format, transports } = require('winston');

const { objectToString } = require('../lib/to_string');

const {
  combine, timestamp, printf, errors,
} = format;

const { LOG_FILE, LOG_LEVEL, LOGS_OFF } = process.env;
const errorFile = LOG_FILE;

const requestLogFormat = printf(({ message: data, level, timestamp: tmstmp }) => {
  const {
    method, url, body, error, message,
  } = data;
  return `[${tmstmp}][${method}:/${url}?${objectToString(body)}] :: ${level}\n${message || error.stack}`;
});

const logger = createLogger({
  level: LOG_LEVEL,
  format: combine(
    timestamp(),
    errors({ stack: true }),
    requestLogFormat,
  ),
  transports: [new transports.File({
    filename: errorFile,
    level: 'info',
  })],
  exitOnError: false,
  silent: LOGS_OFF === 'true',
});

module.exports = logger;
