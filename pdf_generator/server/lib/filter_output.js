const { cloneStructure } = require('./operations');
const { isObject } = require('./checks');

// function filter(output, schema) {
//   let preparedOutput;
//   if (isObject(schema)) {
//     preparedOutput = cloneStructure(schema);
//     Object.keys(preparedOutput).forEach((field) => {
//       preparedOutput[field] = output[field];
//     });
//   } else {
//     preparedOutput = output;
//   }
//   return preparedOutput;
// }

function filter(output, schema) {
  let preparedOutput;
  if (isObject(schema)) {
    preparedOutput = cloneStructure(schema);
    Object.keys(preparedOutput).forEach((field) => {
      if (isObject(preparedOutput[field])) {
        if (!output) preparedOutput[field] = filter({}, preparedOutput[field]);
        else preparedOutput[field] = filter(output[field], preparedOutput[field]);
      } else if (!output) preparedOutput[field] = output;
      else preparedOutput[field] = output[field];
    });
  } else {
    preparedOutput = output;
  }
  return preparedOutput;
}

function findSchema(output, schemaPath, role) {
  const rightSchema = schemaPath[role] || schemaPath.common;
  return filter(output, rightSchema.data);
}

module.exports = findSchema;
