const joi = require('@hapi/joi');

const { BadRequestError } = require('../errors');

const defaultOptions = {
  convert: false,
};

function validate(value, schema, options) {
  const validationOptions = {
    ...defaultOptions,
    ...options,
  };

  const validated = joi.validate(value, schema, validationOptions);

  if (validated.error) throw new BadRequestError(validated.error.message);
  return validated.value;
}

module.exports = validate;
