const { IAM_USER_KEY, IAM_USER_SECRET } = process.env;

module.exports = {
  IAM_USER_KEY,
  IAM_USER_SECRET,
};
