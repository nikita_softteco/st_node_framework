const express = require('express');
const AWS = require('aws-sdk');

const { IAM_USER_KEY, IAM_USER_SECRET } = require('../credentials');
const schemas = require('./schemas');
const validate = require('./validation');

const router = express.Router();

const s3 = new AWS.S3({
  accessKeyId: IAM_USER_KEY,
  secretAccessKey: IAM_USER_SECRET,
});

const download = async function (bucket, key) {
  const options = {
    Bucket: bucket,
    Key: key,
  };

  return s3.getObject(options).promise();
};

const upload = async function (bucket, key, body) {
  const options = {
    Bucket: bucket,
    Key: key,
    Body: body,
  };

  await s3.upload(options).promise();
};

router.get('/pdf', async (req, res, next) => {
  let body;
  try {
    body = validate(req.body, schemas.getPdf);
  } catch (err) {
    return next(err);
  }

  const { filename } = body;
  const data = await download('pdf', filename);
  const file = data.Body.toString();

  res.send(file);
});

router.post('/pdf', async (req, res, next) => {
  let body;
  try {
    body = validate(req.body, schemas.postPdf);
  } catch (err) {
    return next(err);
  }

  const { filename, data } = body;
  await upload('pdf', filename, Buffer.from(data));

  res.end();
});

module.exports = router;
