const express = require('express');

const initApp = require('./app_init');


const PORT = process.env.APP_PORT;

const app = express();
initApp(app);

app.listen(PORT, () => { console.log('AWS Static is running'); });
