function init(app) {
  app.use((req, res) => res.status(404).json({
    error: {
      message: 'Not found',
    },
  }));

  app.use((err, req, res, next) => {
    res.status(err.status || 500).json({
      error: {
        message: err.message,
      },
    });
  });
}

module.exports = init;
