const joi = require('@hapi/joi');

const defaultOptions = {
  convert: false,
};

function validate(value, schema, options) {
  const validationOptions = {
    ...defaultOptions,
    ...options,
  };

  const validated = joi.validate(value, schema, validationOptions);

  if (validated.error) throw validated.error;
  return validated.value;
}

module.exports = validate;
