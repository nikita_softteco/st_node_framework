const joi = require('@hapi/joi');

const schemas = {};

schemas.getPdf = joi.object().keys({
  filename: joi.string().min(15).max(20).required(),
});

schemas.postPdf = joi.object().keys({
  filename: joi.string().min(15).max(20).required(),
  data: joi.binary().required(),
});

module.exports = schemas;
