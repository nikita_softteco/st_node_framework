const express = require('express');

const features = require('./features');

const router = express.Router();

router.post('/send-change-password-email', async (req, res, next) => {
  try {
    await features.sendEmail(req.body);
  } catch (err) {
    return next(err);
  }
  res.end();
});

router.post('/check-token', async (req, res, next) => {
  let response;
  try {
    response = await features.checkToken(req.body);
  } catch (err) {
    return next(err);
  }
  res.json(response);
});

router.post('/throw-token', async (req, res, next) => {
  try {
    await features.throwToken(req.body);
  } catch (err) {
    return next(err);
  }
  res.end();
});

router.get('/spec', async (req, res, next) => {
  let response;
  try {
    response = await features.getSpec();
  } catch (err) {
    return next(err);
  }
  res.json(response);
});

module.exports = router;
