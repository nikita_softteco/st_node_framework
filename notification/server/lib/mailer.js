const nodemailer = require('nodemailer');

const { EMAIL_USER, EMAIL_PASSWORD } = process.env;

const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  auth: {
    user: EMAIL_USER,
    pass: EMAIL_PASSWORD,
  },
});

const defaultMailOptions = {
  from: 'no-reply@mail.ru', // does not work with gmail smtp
};

const mailer = {};

mailer.sendEmail = async function ({ receiver: to, subject, html }) {
  const mailOptions = {
    ...defaultMailOptions,
    to,
    subject,
    html,
  };

  await transporter.sendMail(mailOptions);
};

module.exports = mailer;
