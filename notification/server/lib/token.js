const jwt = require('jsonwebtoken');

const blacklist = require('./redis-async');

const { SECRET, EXP_TIME } = process.env;

const model = {};

model.getToken = function (data) {
  return jwt.sign({
    data,
  },
  SECRET,
  { expiresIn: EXP_TIME });
};

model.checkToken = async function (token) {
  let decoded;
  try {
    decoded = jwt.verify(token, SECRET);
  } catch (err) {
    throw err;
  }

  const isInBlackList = Boolean(await blacklist.get(token));

  if (isInBlackList) {
    const error = new Error();
    error.name = 'TokenInBlacklistError';
    throw error;
  }

  return decoded;
};

model.putToBlacklist = function (token, exp) {
  const expTime = Math.ceil((exp - Date.now() / 1000));
  blacklist.set(token, true, expTime);
};

module.exports = model;
