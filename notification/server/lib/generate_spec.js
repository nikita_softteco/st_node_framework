const fs = require('fs');
const j2s = require('joi-to-swagger');

const inputSchemas = require('../schemas/input_schemas');
const outputSchemas = require('../schemas/responses');
const { isObject, hasDataField } = require('./checks');
const { camel2Dash } = require('./operations');

const resultFile = './swagger.json';

const { OPEN_API_VERSION, SERVICE_TAG, SERVICE_API_VERSION } = process.env;

const openAPI = {
  openapi: OPEN_API_VERSION || '3.0.2',
  info: {
    title: SERVICE_TAG,
    version: SERVICE_API_VERSION || '1.0.0',
  },
};
const pathEntry = {};

function getBodyFromJoi(schema) {
  return j2s(schema).swagger;
}

function getPropertyNamesFromJoi(schema) {
  const { swagger } = j2s(schema);
  return swagger.properties;
}

// MEMO: set path -> method -> requestBody & parameters
Object.keys(inputSchemas).forEach((path) => {
  const formattedPath = camel2Dash(path);
  pathEntry[formattedPath] = {};

  Object.keys(inputSchemas[path]).forEach((method) => {
    pathEntry[formattedPath][method] = {};

    Object.keys(inputSchemas[path][method]).forEach((location) => {
      const schema = inputSchemas[path][method][location];

      switch (location) {
        case 'body': {
          const body = {
            content: {
              'application/json': {
                schema: getBodyFromJoi(schema),
              },
            },
            required: true,
          };

          pathEntry[formattedPath][method].requestBody = body;
        } break;
        case 'query': {
          const params = getPropertyNamesFromJoi(schema);
          const parameters = [];

          Object.keys(params).forEach((name) => {
            parameters.push({
              name,
              in: 'query',
              schema: params[name],
              required: true,
            });
          });
          pathEntry[formattedPath][method].parameters = parameters;
        } break;
        default: {
          throw new Error(`unexpected parameter location: ${location}`);
        }
      }
    });
    pathEntry[formattedPath][method].tags = [SERVICE_TAG];
  });
});

// MEMO: set responses
Object.keys(outputSchemas).forEach((path) => {
  const formattedPath = camel2Dash(path);

  Object.keys(outputSchemas[path]).forEach((method) => {
    const responses = {};
    const okResponse = {};

    const pathResponses = outputSchemas[path][method];
    const { description, data, errors } = pathResponses;

    // MEMO: set 200 response
    okResponse.description = description;

    if (hasDataField(pathResponses)) {
      let mediaType = 'application/json';
      if (!isObject(data)) mediaType = 'text/html';

      const apiSchema = {};
      switch (mediaType) {
        case 'application/json': {
          const properties = {};
          Object.keys(data).forEach((property) => {
            properties[property] = {
              type: typeof data[property],
            };
          });
          apiSchema.properties = properties;
        } break;
        case 'text/html': apiSchema.type = typeof data; break;
        default: {
          throw new Error(`unexpected media type (data): ${mediaType}`);
        }
      }

      okResponse.content = {};
      okResponse.content[`${mediaType}`] = {
        schema: apiSchema,
      };
    }
    responses['200'] = okResponse;

    // MEMO: set error responses
    errors.forEach((error) => {
      responses[error.status] = {
        description: error.message,
      };
    });
    pathEntry[formattedPath][method].responses = responses;
  });
});

openAPI.paths = pathEntry;
fs.writeFileSync(resultFile, JSON.stringify(openAPI, null, 2));
