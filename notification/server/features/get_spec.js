const fs = require('fs');
const util = require('util');

const specFile = './swagger.json';

const asyncReadFile = util.promisify(fs.readFile);

const spec = {};
spec.get = async function get() {
  const contents = await asyncReadFile(specFile, { encoding: 'utf8' });
  return JSON.parse(contents);
};

module.exports = spec.get;
