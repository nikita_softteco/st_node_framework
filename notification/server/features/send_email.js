const pug = require('pug');

const mailer = require('../lib/mailer');
const tokenLib = require('../lib/token');
const schemas = require('../schemas/input_schemas');
const validate = require('../lib/validation');

const { AG_PORT, AG_HOST } = process.env;
const viewsStorage = './views';
const changePasswordEmail = 'change_password_email.pug';

const features = {};

features.send = async function send(body) {
  const request = validate(body, schemas.sendChangePasswordEmail.post.body);
  const { email } = request;

  const token = tokenLib.getToken(email);
  const link = `http://${AG_HOST}:${AG_PORT}/api/auth/password-form?notiftoken=${token}`;
  const compiledEmail = pug.compileFile(`${viewsStorage}/${changePasswordEmail}`);
  const htmlEmail = compiledEmail({ link });
  await mailer.sendEmail({ receiver: email, subject: 'Changing password', html: htmlEmail });
};

module.exports = features.send;
