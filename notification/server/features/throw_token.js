const tokenLib = require('../lib/token');
const schemas = require('../schemas/input_schemas');
const validate = require('../lib/validation');

const features = {};

features.throwToken = async function throwToken(body) {
  const request = validate(body, schemas.throwToken.post.body);
  const { token } = request;
  let decoded;
  try {
    decoded = await tokenLib.checkToken(token);
  } catch (err) {
    if (err.name === 'TokenExpiredError') return;
    throw err;
  }
  tokenLib.putToBlacklist(token, decoded.exp);
};

module.exports = features.throwToken;
