const tokenLib = require('../lib/token');
const schemas = require('../schemas/input_schemas');
const outputSchemas = require('../schemas/output_schemas');
const validate = require('../lib/validation');
const filter = require('../lib/filter_output');
const { UnauthorizedError } = require('../errors');

const features = {};

features.check = async function check(body) {
  const request = validate(body, schemas.checkToken.post.body);

  const { token } = request;
  let decodedToken = {};

  try {
    decodedToken = await tokenLib.checkToken(token);
  } catch (err) {
    if (err.name === 'TokenExpiredError') throw new UnauthorizedError('Link is expired');
    if (err.name === 'TokenInBlacklistError') throw new UnauthorizedError('You have already changed your password');
    throw err;
  }
  return filter(decodedToken, outputSchemas.checkToken.post);
};

module.exports = features.check;
