const joi = require('@hapi/joi');

const structure = require('./api_structure');

const { cloneStructure } = require('../lib/operations');

const schemas = cloneStructure(structure);

schemas.sendChangePasswordEmail.post.body = joi.object().keys({
  email: joi.string().email().required(),
});

schemas.checkToken.post.body = joi.object().keys({
  token: joi.string().regex(/[0-9a-zA-Z.]+/).required(),
});

schemas.throwToken.post.body = joi.object().keys({
  token: joi.string().regex(/[0-9a-zA-Z.]+/).required(),
});

module.exports = schemas;
