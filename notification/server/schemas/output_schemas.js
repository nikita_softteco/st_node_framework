const structure = require('./api_structure');

const { cloneStructure } = require('../lib/operations');

const schemas = cloneStructure(structure);

schemas.checkToken.post = {
  common: {
    data: {
      data: '',
    },
  },
};

module.exports = schemas;
