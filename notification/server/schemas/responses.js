const structure = require('./api_structure');
const outputSchemas = require('./output_schemas');
const { BadRequestError, UnauthorizedError } = require('../errors');

const { cloneStructure, copyDataFieldBetweenSchemas } = require('../lib/operations');

const schemas = cloneStructure(structure);

schemas.sendChangePasswordEmail.post = {
  description: 'Sends an email for password change',
  errors: [new BadRequestError()],
};

schemas.checkToken.post = {
  description: 'Returns decoded token',
  errors: [new BadRequestError(), new UnauthorizedError('Invalid token, could be expired or used')],
};

schemas.throwToken.post = {
  description: 'Make token invalid',
  errors: [new BadRequestError(), new UnauthorizedError('Invalid token, could be expired or used')],
};

const responseSchema = copyDataFieldBetweenSchemas(outputSchemas, schemas);

module.exports = responseSchema;
