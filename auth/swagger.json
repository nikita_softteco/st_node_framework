{
  "openapi": "3.0.2",
  "info": {
    "title": "auth",
    "version": "1.0.0"
  },
  "paths": {
    "/user": {
      "post": {
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "required": [
                  "username",
                  "password",
                  "email",
                  "role"
                ],
                "properties": {
                  "username": {
                    "type": "string",
                    "minLength": 5,
                    "maxLength": 15
                  },
                  "password": {
                    "type": "string",
                    "minLength": 5,
                    "maxLength": 15
                  },
                  "email": {
                    "type": "string",
                    "format": "email"
                  },
                  "role": {
                    "type": "integer"
                  }
                }
              }
            }
          },
          "required": true
        },
        "tags": [
          "auth"
        ],
        "responses": {
          "200": {
            "description": "Returns created user",
            "content": {
              "application/json": {
                "schema": {
                  "properties": {
                    "id": {
                      "type": "string"
                    },
                    "username": {
                      "type": "string"
                    },
                    "email": {
                      "type": "string"
                    },
                    "role": {
                      "type": "number"
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Data validation failed"
          }
        }
      }
    },
    "/login": {
      "post": {
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "required": [
                  "username",
                  "password"
                ],
                "properties": {
                  "username": {
                    "type": "string",
                    "minLength": 5,
                    "maxLength": 15
                  },
                  "password": {
                    "type": "string",
                    "minLength": 5,
                    "maxLength": 15
                  }
                }
              }
            }
          },
          "required": true
        },
        "tags": [
          "auth"
        ],
        "responses": {
          "200": {
            "description": "Returns new access token",
            "content": {
              "application/json": {
                "schema": {
                  "properties": {
                    "token": {
                      "type": "string"
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Data validation failed"
          },
          "401": {
            "description": "Invalid username or password"
          }
        }
      }
    },
    "/logout": {
      "post": {
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "required": [
                  "token"
                ],
                "properties": {
                  "token": {
                    "type": "string",
                    "pattern": "[0-9a-zA-Z.]+"
                  }
                }
              }
            }
          },
          "required": true
        },
        "tags": [
          "auth"
        ],
        "responses": {
          "200": {
            "description": "Returns old access token",
            "content": {
              "application/json": {
                "schema": {
                  "properties": {
                    "token": {
                      "type": "string"
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Data validation failed"
          },
          "401": {
            "description": "Invalid token"
          }
        }
      }
    },
    "/check": {
      "post": {
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "required": [
                  "token"
                ],
                "properties": {
                  "token": {
                    "type": "string",
                    "pattern": "[0-9a-zA-Z.]+"
                  }
                }
              }
            }
          },
          "required": true
        },
        "tags": [
          "auth"
        ],
        "responses": {
          "200": {
            "description": "Checks whether provided token is valid",
            "content": {
              "application/json": {
                "schema": {
                  "properties": {
                    "isValid": {
                      "type": "boolean"
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Data validation failed"
          }
        }
      }
    },
    "/change-password": {
      "post": {
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "required": [
                  "email"
                ],
                "properties": {
                  "email": {
                    "type": "string",
                    "format": "email"
                  }
                }
              }
            }
          },
          "required": true
        },
        "tags": [
          "auth"
        ],
        "responses": {
          "200": {
            "description": "Asks notification service to send an email with link to change password"
          },
          "400": {
            "description": "Data validation failed"
          },
          "401": {
            "description": "There are no users with this email"
          }
        }
      }
    },
    "/password-form": {
      "get": {
        "parameters": [
          {
            "name": "token",
            "in": "query",
            "schema": {
              "type": "string",
              "pattern": "[0-9a-zA-Z.]+"
            },
            "required": true
          }
        ],
        "tags": [
          "auth"
        ],
        "responses": {
          "200": {
            "description": "Send form to fill new password",
            "content": {
              "text/html": {
                "schema": {
                  "type": "string"
                }
              }
            }
          },
          "400": {
            "description": "Data validation failed"
          },
          "401": {
            "description": "Invalid token, could be expired or used"
          }
        }
      }
    },
    "/new-password": {
      "post": {
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "required": [
                  "password",
                  "confirmPassword"
                ],
                "properties": {
                  "password": {
                    "type": "string",
                    "minLength": 5,
                    "maxLength": 15
                  },
                  "confirmPassword": {
                    "type": "string",
                    "minLength": 5,
                    "maxLength": 15
                  }
                }
              }
            }
          },
          "required": true
        },
        "parameters": [
          {
            "name": "token",
            "in": "query",
            "schema": {
              "type": "string",
              "pattern": "[0-9a-zA-Z.]+"
            },
            "required": true
          }
        ],
        "tags": [
          "auth"
        ],
        "responses": {
          "200": {
            "description": "Save new password"
          },
          "400": {
            "description": "Data validation failed"
          },
          "401": {
            "description": "Invalid token, could be expired or used"
          }
        }
      }
    }
  }
}