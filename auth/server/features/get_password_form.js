const pug = require('pug');

const validate = require('../lib/validation');
const filter = require('../lib/filter_output');
const schemas = require('../schemas/input_schemas');
const outputSchemas = require('../schemas/output_schemas');
const { UnauthorizedError } = require('../errors');
const notificationService = require('../services/notification');

const viewsStorage = './views';
const compiledPassworForm = pug.compileFile(`${viewsStorage}/new-password-form.pug`);
const { AG_HOST, AG_PORT } = process.env;

const features = {};

features.get = async function get(query) {
  const { notiftoken: token } = validate(query, schemas.passwordForm.get.query);
  try {
    await notificationService.checkToken({ token });
  } catch (err) {
    if (err.status === 401) throw new UnauthorizedError(err.message);
    throw err;
  }
  const action = `http://${AG_HOST}:${AG_PORT}/api/auth/new-password?notiftoken=${token}`;
  const form = compiledPassworForm({ action });

  return filter(form, outputSchemas.passwordForm.get);
};

module.exports = features.get;
