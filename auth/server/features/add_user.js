const userModel = require('../db/queries/users');
const roleModel = require('../db/queries/roles');

const schemas = require('../schemas/input_schemas');
const outputSchemas = require('../schemas/output_schemas');
const validate = require('../lib/validation');
const filter = require('../lib/filter_output');
const { BadRequestError, UnauthorizedError } = require('../errors');

const features = {};

features.addUser = async function add(body, query) {
  if (!query.meta) throw new UnauthorizedError('No metadata about current user');
  const meta = JSON.parse(query.meta);

  const request = validate(body, schemas.user.post.body);
  const role = await roleModel.findRole({ id: request.role });
  if (!role) throw new BadRequestError('No such role');

  const savedUser = await userModel.saveUser(request);
  return filter(savedUser, outputSchemas.user.post, meta.role.name);
};

module.exports = features.addUser;
