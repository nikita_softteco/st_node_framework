const userModel = require('../db/queries/users');
const schemas = require('../schemas/input_schemas');
const validate = require('../lib/validation');
const { UnauthorizedError, BadRequestError } = require('../errors');
const notificationService = require('../services/notification');

const features = {};

features.set = async function set(query, body) {
  const { notiftoken: token } = validate(query, schemas.newPassword.post.query);
  const { password, confirmPassword } = validate(body, schemas.newPassword.post.body);

  let response;
  try {
    response = await notificationService.checkToken({ token });
  } catch (err) {
    if (err.status === 401) throw new UnauthorizedError(err.message);
    throw err;
  }
  const decodedToken = response.body;
  if (password !== confirmPassword) throw new BadRequestError('Passwords are not equal');
  await userModel.updateUser({ email: decodedToken.data }, { password });
  await notificationService.throwToken({ token });
};

module.exports = features.set;
