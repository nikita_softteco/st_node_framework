const userModel = require('../db/queries/users');
const validate = require('../lib/validation');
const schemas = require('../schemas/input_schemas');
const { UnauthorizedError } = require('../errors');
const notificationService = require('../services/notification');

const features = {};

features.change = async function change(body) {
  const { email } = validate(body, schemas.changePassword.post.body);
  const user = await userModel.findUser({ email });
  if (!user) throw new UnauthorizedError('There are no users with this email');

  await notificationService.sendChangePasswordEmail({ email });
};

module.exports = features.change;
