const jwt = require('jsonwebtoken');

const tokensDB = require('../lib/redis-async');
const userModel = require('../db/queries/users');
const roleModel = require('../db/queries/roles');
const validate = require('../lib/validation');
const filter = require('../lib/filter_output');
const schemas = require('../schemas/input_schemas');
const outputSchemas = require('../schemas/output_schemas');

const { SECRET } = process.env;

const features = {};

features.check = async function check(query) {
  const { token } = validate(query, schemas.check.get.query);
  let decoded;
  try {
    decoded = jwt.verify(token, SECRET);
  } catch (err) {
    return filter({ isValid: false }, outputSchemas.check.get);
  }

  const response = { isValid: false };
  const isInBlackList = await tokensDB.exists(token);

  if (!isInBlackList) {
    const user = await userModel.findUser({
      _id: decoded.id,
      salt: decoded.salt,
    });
    if (user) {
      response.isValid = true;
      const { id, name } = await roleModel.findRole({ id: user.role });
      response.meta = {
        id: user.id,
        role: {
          id,
          name,
        },
      };
    }
  }

  return filter(response, outputSchemas.check.get);
};

module.exports = features.check;
