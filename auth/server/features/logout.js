const jwt = require('jsonwebtoken');

const tokensDB = require('../lib/redis-async');
const validate = require('../lib/validation');
const filter = require('../lib/filter_output');
const schemas = require('../schemas/input_schemas');
const outputSchemas = require('../schemas/output_schemas');
const { UnauthorizedError } = require('../errors');

const { SECRET } = process.env;

const features = {};

features.logout = function logout(body, query) {
  if (!query.meta) throw new UnauthorizedError('No metadata about current user');

  const { token } = validate(body, schemas.logout.post.body);
  let decoded;
  try {
    decoded = jwt.verify(token, SECRET);
  } catch (err) {
    throw new UnauthorizedError(err.message);
  }
  const expTime = Math.ceil((decoded.exp - Date.now() / 1000));
  tokensDB.set(token, true, expTime);

  return filter({ token }, outputSchemas.logout.post);
};

module.exports = features.logout;
