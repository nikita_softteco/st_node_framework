const addUser = require('./add_user');
const login = require('./login');
const logout = require('./logout');
const checkToken = require('./check_token');
const changePassword = require('./change_password');
const getForm = require('./get_password_form');
const setPassword = require('./set_new_password');
const getSpec = require('./get_spec');

module.exports = {
  addUser,
  login,
  logout,
  checkToken,
  changePassword,
  getForm,
  setPassword,
  getSpec,
};
