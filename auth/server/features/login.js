const validate = require('../lib/validation');
const filter = require('../lib/filter_output');
const schemas = require('../schemas/input_schemas');
const outputSchemas = require('../schemas/output_schemas');
const { UnauthorizedError } = require('../errors');

const features = {};

features.check = function check(body) {
  validate(body, schemas.login.post.body);
};

features.handleAuth = function handle(err, passportUser, info) {
  if (err) throw err;
  if (!passportUser) throw new UnauthorizedError(info);

  const token = passportUser.generateJWT();
  return filter({ token }, outputSchemas.login.post);
};

module.exports = features;
