const bodyParser = require('body-parser');

function init(app) {
  app.use(bodyParser.json());
}

module.exports = init;
