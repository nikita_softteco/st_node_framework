require('./init_configs');
const initParsers = require('./init_parsers');
const initRoutes = require('./init_routes');
const initErrorHanlers = require('./init_error_handlers');

function init(app) {
  initParsers(app);
  initRoutes(app);
  initErrorHanlers(app);
}

module.exports = init;
