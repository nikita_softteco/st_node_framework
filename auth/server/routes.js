const passport = require('passport');
const express = require('express');

const features = require('./features');

const router = express.Router();

router.post('/user', async (req, res, next) => {
  let response;
  try {
    response = await features.addUser(req.body, req.query);
  } catch (err) {
    return next(err);
  }
  res.json(response);
});

router.post('/login', (req, res, next) => {
  try {
    features.login.check(req.body);
  } catch (err) {
    return next(err);
  }
  let response;
  passport.authenticate('local', { session: false }, (err, passportUser, info) => {
    try {
      response = features.login.handleAuth(err, passportUser, info);
    } catch (e) {
      return next(e);
    }
    return res.json(response);
  })(req, res, next);
});

router.post('/logout', (req, res, next) => {
  let response;
  try {
    response = features.logout(req.body, req.query);
  } catch (err) {
    return next(err);
  }
  res.json(response);
});

router.get('/check', async (req, res, next) => {
  let response;
  try {
    response = await features.checkToken(req.query);
  } catch (err) {
    return next(err);
  }
  res.json(response);
});

router.post('/change-password', async (req, res, next) => {
  try {
    await features.changePassword(req.body);
  } catch (err) {
    return next(err);
  }
  res.end();
});

router.get('/password-form', async (req, res, next) => {
  let response;
  try {
    response = await features.getForm(req.query);
  } catch (err) {
    return next(err);
  }
  res.end(response);
});

router.post('/new-password', async (req, res, next) => {
  try {
    await features.setPassword(req.query, req.body);
  } catch (err) {
    return next(err);
  }
  res.end();
});

router.get('/spec', async (req, res, next) => {
  let response;
  try {
    response = await features.getSpec();
  } catch (err) {
    return next(err);
  }
  res.json(response);
});

module.exports = router;
