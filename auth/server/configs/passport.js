const passport = require('passport');
const LocalStrategy = require('passport-local');

const userModel = require('../db/queries/users');

// TODO: change strategy to jwt or smth
passport.use(new LocalStrategy(async (username, password, done) => {
  let user;
  try {
    user = await userModel.findUserByCredentials(username, password);
  } catch (err) {
    return done(err);
  }

  if (!user) {
    return done(null, false, 'Username or password is invalid');
  }

  return done(null, user);
}));
