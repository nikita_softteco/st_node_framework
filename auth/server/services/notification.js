const Service = require('./service');

const { NOTIFICATION_HOST, NOTIFICATION_PORT } = process.env;
const defaultMethod = 'POST';

const service = new Service(NOTIFICATION_HOST, NOTIFICATION_PORT, defaultMethod);

const request = {};

request.sendChangePasswordEmail = async function (email) {
  const options = {
    url: '/send-change-password-email',
    data: email,
  };
  return service.sendRequest(options);
};

request.checkToken = async function (token) {
  const options = {
    url: '/check-token',
    data: token,
  };
  return service.sendRequest(options);
};

request.throwToken = async function (token) {
  const options = {
    url: '/throw-token',
    data: token,
  };
  return service.sendRequest(options);
};

module.exports = request;
