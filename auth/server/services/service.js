const axios = require('axios');

const { HttpError } = require('../errors');

class Service {
  constructor(host, port, method) {
    this.baseURL = `http://${host}:${port}`;
    this.defaultMethod = method;
  }

  async sendRequest(options) {
    const reqOptions = {
      baseURL: this.baseURL,
      method: this.defaultMethod,
      ...options,
    };

    let res;
    try {
      res = await axios(reqOptions);
    } catch (error) {
      if (error.response) {
        throw new HttpError({
          status: error.response.status,
          message: error.response.data.error.message,
        });
      }
      if (error.message) throw new HttpError({ message: error.message });
    }

    return {
      status: res.status,
      body: res.data,
    };
  }
}

module.exports = Service;
