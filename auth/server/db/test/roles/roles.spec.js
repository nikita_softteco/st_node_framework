const assert = require('assert');

const fixture = require('./fixture');
const roleModel = require('../../models/roles');

describe('MODEL::Role', () => {
  before(async () => {
    await fixture.addRoles();
  });
  describe('#findRole', () => {
    it('should return role by its id', async () => {
      const selector = {
        id: 0,
      };
      const expectedResult = {
        id: 0,
        name: 'someone',
        description: 'some permissions',
      };
      const actualResult = await roleModel.findRole(selector);

      assert.deepEqual(expectedResult, actualResult.getValues());
    });
    it('should return role by its name', async () => {
      const selector = {
        name: 'admin',
      };
      const expectedResult = {
        id: 3,
        name: 'admin',
        description: 'can manage data',
      };
      const actualResult = await roleModel.findRole(selector);

      assert.deepEqual(expectedResult, actualResult.getValues());
    });
    it('should return null for not existing role id', async () => {
      const selector = {
        id: 6,
      };
      const expectedResult = null;
      const actualResult = await roleModel.findRole(selector);

      assert.deepEqual(expectedResult, actualResult);
    });
  });
});
