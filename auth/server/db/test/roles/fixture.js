const util = require('util');
const exec = util.promisify(require('child_process').exec);

const { MONGO_HOST } = process.env;
const addRolesFile = '/usr/src/app/server/db/models/roles/test/create_collection_roles';

const fixture = {};

fixture.addRoles = async function () {
  const { stderr } = await exec(`mongo --host ${MONGO_HOST} --port 27017 users_test < ${addRolesFile}`);
  if (stderr) {
    console.log(stderr);
    process.exit();
  }
};

module.exports = fixture;
