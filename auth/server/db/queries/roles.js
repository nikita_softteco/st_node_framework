const mongoose = require('mongoose');

const roleModel = require('../models/roles');

const Role = mongoose.model('Role', roleModel);

const queries = {};

queries.findRole = async function (selector) {
  return Role.findOne(selector);
};

module.exports = queries;
