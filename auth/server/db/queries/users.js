const mongoose = require('mongoose');

const hashLib = require('../../lib/hash');
const userModel = require('../models/users');

const User = mongoose.model('User', userModel);

const queries = {};

queries.findUser = async function (selector) {
  return User.findOne(selector);
};

queries.findUserByCredentials = async function (username, password) {
  const user = await queries.findUser({ username });
  if (!user) return null;
  const isRightPassword = user.checkPassword(password);
  if (isRightPassword) return user;
  return null;
};

queries.updateUser = async function (filter, update) {
  const passwordFields = {};
  if (update.password) {
    passwordFields.salt = hashLib.createHash();
    passwordFields.password = hashLib.hash(update.password, passwordFields.salt);
  }
  const newData = {
    ...update,
    ...passwordFields,
  };

  const res = await User.updateOne(filter, newData);
  return res.nModified || res.n;
};

queries.saveUser = async function (user) {
  const salt = hashLib.createHash();
  const hashedPassword = hashLib.hash(user.password, salt);
  const newData = {
    ...user,
    password: hashedPassword,
    salt,
  };
  const [inserted] = await User.insertMany([newData]);
  return inserted;
};

module.exports = queries;
