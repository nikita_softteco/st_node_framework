const mongoose = require('mongoose');

const roleSchema = new mongoose.Schema({
  id: Number,
  name: String,
  description: String,
}, {
  versionKey: false,
});

roleSchema.methods.getValues = function () {
  return {
    id: this.id,
    name: this.name,
    description: this.description,
  };
};

module.exports = roleSchema;
