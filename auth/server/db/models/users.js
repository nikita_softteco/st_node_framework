const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const { hash } = require('../../lib/hash');

const { SECRET: GLOBAL_SECRET, TOKEN_EXP_TIME } = process.env;

const userSchema = new mongoose.Schema({
  username: String,
  role: Number,
  email: String,
  password: String,
  salt: String,
}, {
  versionKey: false,
});

userSchema.methods.getValues = function () {
  return {
    id: this.id,
    username: this.username,
    role: this.role,
    email: this.email,
  };
};

userSchema.methods.generateJWT = function () {
  return jwt.sign({
    id: this.id,
    salt: this.salt,
  },
  GLOBAL_SECRET,
  { expiresIn: TOKEN_EXP_TIME });
};

userSchema.methods.checkPassword = function (password) {
  const hashedPassword = hash(password, this.salt);
  return this.password === hashedPassword;
};

module.exports = userSchema;
