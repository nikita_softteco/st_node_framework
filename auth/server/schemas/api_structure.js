const schemas = {
  user: {
    post: {},
  },
  login: {
    post: {},
  },
  logout: {
    post: {},
  },
  check: {
    get: {},
  },
  changePassword: {
    post: {},
  },
  passwordForm: {
    get: {},
  },
  newPassword: {
    post: {},
  },
};

module.exports = schemas;
