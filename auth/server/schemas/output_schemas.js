const structure = require('./api_structure');

const { cloneStructure } = require('../lib/operations');

const schemas = cloneStructure(structure);

schemas.user.post = {
  admin: {
    data: {
      id: '',
      username: '',
      email: '',
      role: -1,
    },
  },
  common: {
    data: {
      username: '',
      email: '',
    },
  },
};

schemas.login.post = {
  common: {
    data: {
      token: '',
    },
  },
};

schemas.logout.post = {
  common: {
    data: {
      token: '',
    },
  },
};

schemas.check.get = {
  common: {
    data: {
      isValid: false,
      meta: {
        id: -1,
        role: {
          id: -1,
          name: 'role',
        },
      },
    },
  },
};

schemas.passwordForm.get = {
  common: {
    data: '',
  },
};

module.exports = schemas;
