const structure = require('./api_structure');
const outputSchemas = require('./output_schemas');
const { BadRequestError, UnauthorizedError } = require('../errors');

const { cloneStructure, copyDataFieldBetweenSchemas } = require('../lib/operations');

const schemas = cloneStructure(structure);

schemas.user.post = {
  description: 'Returns created user',
  errors: [new BadRequestError()],
};

schemas.login.post = {
  description: 'Returns new access token',
  errors: [new BadRequestError(), new UnauthorizedError('Invalid username or password')],
};

schemas.logout.post = {
  description: 'Returns old access token',
  errors: [new BadRequestError(), new UnauthorizedError('Invalid token')],
};

schemas.check.get = {
  description: 'Checks whether provided token is valid; if yes - returns meta info',
  errors: [new BadRequestError()],
};

schemas.changePassword.post = {
  description: 'Asks notification service to send an email with link to change password',
  errors: [new BadRequestError(), new UnauthorizedError('There are no users with this email')],
};

schemas.passwordForm.get = {
  description: 'Send form to fill new password',
  errors: [new BadRequestError(), new UnauthorizedError('Invalid token, could be expired or used')],
};

schemas.newPassword.post = {
  description: 'Save new password',
  errors: [new BadRequestError(), new UnauthorizedError('Invalid token, could be expired or used')],
};

const responseSchema = copyDataFieldBetweenSchemas(outputSchemas, schemas);

module.exports = responseSchema;
