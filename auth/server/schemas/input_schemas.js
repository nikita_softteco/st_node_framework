const joi = require('@hapi/joi');

const structure = require('./api_structure');

const { cloneStructure } = require('../lib/operations');

const schemas = cloneStructure(structure);

schemas.user.post.body = joi.object().keys({
  username: joi.string().min(5).max(15).required(),
  password: joi.string().min(5).max(15).required(),
  email: joi.string().email().required(),
  role: joi.number().integer().required(),
});

schemas.login.post.body = joi.object().keys({
  username: joi.string().min(5).max(15).required(),
  password: joi.string().min(5).max(15).required(),
});

schemas.logout.post.body = joi.object().keys({
  token: joi.string().regex(/[0-9a-zA-Z.]+/).required(),
});

schemas.check.get.query = schemas.logout.post.body;

schemas.changePassword.post.body = joi.object().keys({
  email: joi.string().email().required(),
});

schemas.passwordForm.get.query = joi.object().keys({
  notiftoken: joi.string().regex(/[0-9a-zA-Z.]+/).required(),
});

schemas.newPassword.post.body = joi.object().keys({
  password: joi.string().min(5).max(15).required(),
  confirmPassword: joi.string().min(5).max(15).required(),
});

schemas.newPassword.post.query = schemas.passwordForm.get.query;

module.exports = schemas;
