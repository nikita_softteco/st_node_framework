const checks = {};

checks.isObject = function isObject(value) {
  const type = typeof value;
  if (type !== 'object' || value === null || Array.isArray(value)) return false;
  return true;
};

checks.isArray = function isArray(value) {
  if (Array.isArray(value)) return true;
  return false;
};

checks.hasDataField = function hasDataField(obj) {
  return Object.keys(obj).includes('data');
};

checks.isNotEmpty = function hasSchema(obj) {
  return !!Object.keys(obj).length;
};

module.exports = checks;
