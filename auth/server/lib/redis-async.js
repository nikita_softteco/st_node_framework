const redis = require('redis');
const util = require('util');

const asyncRedisGet = util.promisify(redis.RedisClient.prototype.get);
const asyncRedisExists = util.promisify(redis.RedisClient.prototype.exists);

const { REDIS_HOST, REDIS_PORT, REDIS_DATABASE_NUMBER } = process.env;
const connectionOptions = {
  host: REDIS_HOST,
  port: REDIS_PORT,
};

const DATABASE_NUMBER = REDIS_DATABASE_NUMBER;

const model = {};

model.set = function (key, value, exp) {
  const client = redis.createClient(connectionOptions);
  client.select(DATABASE_NUMBER);
  client.set(key, value, 'EX', exp);
  client.quit();
};

model.get = async function (key) {
  const client = redis.createClient(connectionOptions);
  client.select(DATABASE_NUMBER);
  const value = await asyncRedisGet.call(client, key);
  client.quit();
  return value;
};

model.exists = async function (key) {
  const client = redis.createClient(connectionOptions);
  client.select(DATABASE_NUMBER);
  const value = await asyncRedisExists.call(client, key);
  client.quit();
  return Boolean(value);
};

module.exports = model;
