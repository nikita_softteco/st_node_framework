const { isNotEmpty } = require('./checks');

const ops = {};

ops.cloneStructure = function cloneStructure(obj) {
  const clone = {};
  Object.keys(obj).forEach((key) => {
    if (typeof (obj[key]) === 'object' && obj[key] !== null) clone[key] = cloneStructure(obj[key]);
    else clone[key] = obj[key];
  });
  return clone;
};

ops.copyDataFieldBetweenSchemas = function copyDataFieldBetweenSchemas(srcSchema, destSchema) {
  const outputSchema = destSchema;
  Object.keys(outputSchema).forEach((path) => {
    Object.keys(outputSchema[path]).forEach((method) => {
      if (isNotEmpty(srcSchema[path][method])) {
        outputSchema[path][method].data = Object.keys(srcSchema[path][method])
          .reduce((data, schemaName) => {
            return {
              ...data,
              ...srcSchema[path][method][schemaName].data,
            };
          }, {});
      }
    });
  });
  return outputSchema;
};

ops.camel2Dash = function camel2Dash(path) {
  const array = path.split('');
  array.forEach((letter, index) => {
    if (letter !== letter.toLowerCase()) array.splice(index, 1, '-', letter.toLowerCase());
  });
  return `/${array.join('')}`;
};

module.exports = ops;
