const crypto = require('crypto');

const lib = {};

lib.createHash = function () {
  const hash = crypto.createHash('sha256');

  hash.update(Date.now().toString());
  return hash.digest('hex');
};

lib.hash = function(data, salt) {
  const hash = crypto.createHash('sha256');

  hash.update(`${data}${salt}`);
  return hash.digest('hex');
};

module.exports = lib;
