module.exports = {
  apps : [{
    name: "app",
    script: "server/index.js",
    exec_mode: "cluster",
    instances: "max",
    watch: true,
    env: {
      NODE_ENV: "development",
    },
    env_production: {
      NODE_ENV: "production",
    }
  }]
};