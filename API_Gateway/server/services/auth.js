const Service = require('./service');

const { AUTH_HOST, AUTH_PORT } = process.env;
const service = new Service(AUTH_HOST, AUTH_PORT);

module.exports = service;
