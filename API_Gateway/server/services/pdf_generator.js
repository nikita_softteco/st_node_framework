const Service = require('./service');

const { PDF_GENERATOR_HOST, PDF_GENERATOR_PORT } = process.env;
const service = new Service(PDF_GENERATOR_HOST, PDF_GENERATOR_PORT);

module.exports = service;
