const axios = require('axios');

const { HttpError } = require('../assets/errors');

class Service {
  constructor(host, port) {
    this.baseURL = `http://${host}:${port}`;
  }

  async sendRequest(options) {
    const reqOptions = {
      baseURL: this.baseURL,
      ...options,
    };

    let res;
    try {
      res = await axios(reqOptions);
    } catch (error) {
      if (error.response) {
        const { status, data } = error.response;
        let parsedData;
        if (data instanceof Buffer) parsedData = JSON.parse(data.toString());
        else parsedData = data;
        throw new HttpError({
          status,
          message: parsedData.error.message,
        });
      }
      if (error.message) throw new HttpError({ message: error.message });
    }

    return {
      status: res.status,
      body: res.data,
    };
  }
}

module.exports = Service;
