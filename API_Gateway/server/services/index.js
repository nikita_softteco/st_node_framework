const authServise = require('./auth');
const pdfGeneratorServise = require('./pdf_generator');

module.exports = {
  authServise,
  pdfGeneratorServise,
};
