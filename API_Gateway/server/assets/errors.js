class HttpError extends Error {
  constructor({ status = 500, message = 'Something went wrong in service' }) {
    super(message);
    this.status = status;
  }
}

class UnauthorizedError extends HttpError {
  constructor(message = 'Invalid credentials') {
    super({ status: 401, message });
    this.name = 'Unauthorized';
  }
}

class ForbiddenError extends HttpError {
  constructor(message = 'Access denied') {
    super({ status: 403, message });
    this.name = 'Forbidden';
  }
}

module.exports = {
  HttpError,
  UnauthorizedError,
  ForbiddenError,
};
