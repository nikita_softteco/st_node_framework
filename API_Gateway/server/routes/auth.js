const express = require('express');

const auth = require('../utils/authorize');
const { handler } = require('../utils/handlers/auth');
const brute = require('../utils/brute');

const router = express.Router();

router.post('/user', brute.prevent, auth.required, handler);
router.post('/login', handler);
router.post('/logout', brute.prevent, auth.required, handler);
router.post('/change-password', handler);
router.get('/password-form', handler);
router.post('/new-password', handler);

module.exports = router;
