const express = require('express');
const { handler, getPDF } = require('../utils/handlers/pdf_generator');
const auth = require('../utils/authorize');
const brute = require('../utils/brute');

const router = express.Router();

router.post('*', brute.prevent, auth.required, handler);
router.get('*', getPDF);

module.exports = router;
