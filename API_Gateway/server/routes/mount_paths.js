module.exports = {
  auth: '/api/auth',
  pdfGenerator: '/api/pdf',
  spec: '/spec',
};
