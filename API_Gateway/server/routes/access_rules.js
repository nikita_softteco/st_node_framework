const paths = require('./mount_paths');

const rulls = {};

rulls[paths.auth] = {
  '/user': ['admin', 'superuser'],
  '/logout': ['someone', 'worker', 'superuser'],
};

rulls[paths.pdfGenerator] = {
  all: ['someone', 'worker', 'superuser'],
};

module.exports = rulls;
