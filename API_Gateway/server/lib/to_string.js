const { isObject, isArray } = require('./type_check');

function arrayToString(array) {
  const interArray = [];
  array.forEach((element) => {
    if (isObject(element)) interArray.push(objectToString(element));
    else if (isArray(element)) interArray.push(arrayToString(element));
    else interArray.push(element);
  });
  return `[${interArray.join(', ')}]`;
}

function objectToString(obj) {
  if (!obj) return obj;
  const array = [];
  Object.keys(obj).forEach((prop) => {
    if (isObject(obj[prop])) array.push(`${prop}: ${objectToString(obj[prop])}`);
    else if (isArray(obj[prop])) array.push(`${prop}: ${arrayToString(obj[prop])}`);
    else array.push(`${prop}: ${obj[prop]}`);
  });
  return `{${array.join(', ')}}`;
}

module.exports = {
  arrayToString,
  objectToString,
};
