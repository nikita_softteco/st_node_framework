const checks = {};

checks.isObject = function isObject(value) {
  const type = typeof value;
  if (type !== 'object' || value === null || Array.isArray(value)) return false;
  return true;
};

checks.isArray = function isArray(value) {
  if (Array.isArray(value)) return true;
  return false;
};

module.exports = checks;
