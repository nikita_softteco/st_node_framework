const ExpressBrute = require('express-brute');
const RedisStore = require('express-brute-redis');

const { REDIS_HOST, REDIS_PORT } = process.env;

const store = new RedisStore({
  host: REDIS_HOST,
  port: REDIS_PORT,
});
const bruteforce = new ExpressBrute(store);

module.exports = bruteforce;
