const service = require('../../services/pdf_generator');
const Forwarder = require('../forwarder');

const forwarder = new Forwarder(service);

const handlers = {};

handlers.handler = async (req, res, next) => {
  let response;
  try {
    response = await forwarder.forward(req);
  } catch (err) {
    return next(err);
  }
  res.send(response);
};

handlers.getPDF = async (req, res, next) => {
  let response;
  try {
    response = await forwarder.forward(req, { responseType: 'arraybuffer' });
  } catch (err) {
    return next(err);
  }

  res.set('Content-Type', 'application/pdf');
  res.set('Content-Disposition', 'attachment; filename="cool.pdf"');
  res.send(response);
};

module.exports = handlers;
