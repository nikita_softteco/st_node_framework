const service = require('../../services/auth');
const Forwarder = require('../forwarder');

const forwarder = new Forwarder(service);

const handlers = {};

handlers.handler = async (req, res, next) => {
  let response;
  try {
    response = await forwarder.forward(req);
  } catch (err) {
    return next(err);
  }
  res.send(response);
};

module.exports = handlers;
