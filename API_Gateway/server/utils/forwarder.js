class Forwarder {
  constructor(service) {
    this.service = service;
  }

  async forward({
    method, path, query, body,
  }, options) {
    const reqOptions = {
      method,
      url: path,
      params: query,
      data: body,
      ...options,
    };
    const response = await this.service.sendRequest(reqOptions);
    return response.body;
  }
}

module.exports = Forwarder;
