const service = require('../services/auth');
const { UnauthorizedError, ForbiddenError } = require('../assets/errors');
const rules = require('../routes/access_rules');

const auth = {};

auth.required = async function check(req, res, next) {
  const { token } = req.query;
  if (!token) return next(new UnauthorizedError('No token found'));

  const options = {
    method: 'get',
    url: '/check',
    params: { token },
  };
  let response;
  try {
    response = await service.sendRequest(options);
  } catch (err) {
    return next(err);
  }
  const { isValid, meta } = response.body;
  if (!isValid) return next(new UnauthorizedError('Invalid token'));

  const rule = rules[req.baseUrl] && (rules[req.baseUrl][req.path] || rules[req.baseUrl].all);
  if (!rule) {
    delete req.query.token;
    return next();
  }

  if (rule.includes(meta.role.name)) {
    delete req.query.token;
    req.query.meta = meta;
    return next();
  }
  return next(new ForbiddenError(`No access for role ${meta.role.name}`));
};

module.exports = auth;
