const services = require('../../services');
const mountPaths = require('../../routes/mount_paths');

const spec = {
  openapi: '3.0.2',
  info: {
    title: 'API Gateway: entry for all requests',
    version: '1.0.0',
  },
};

const collectSpec = async function collect(req, res, next) {
  const pathEntry = {};
  const requestsArray = [];

  Object.keys(services).forEach((serviceName) => {
    requestsArray.push(services[serviceName].sendRequest({ method: 'get', url: '/spec' }));
  });
  Promise.all(requestsArray)
    .then((responses) => {
      responses.forEach((response) => {
        const api = response.body;
        const serviceTag = api.info.title;
        const { paths } = api;

        Object.keys(paths).forEach((path) => {
          pathEntry[`${mountPaths[serviceTag]}${path}`] = paths[path];
        });
      });
      spec.paths = pathEntry;
      return res.json(spec);
    })
    .catch(err => next(err));
};

module.exports = collectSpec;
