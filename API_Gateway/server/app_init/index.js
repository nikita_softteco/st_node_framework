const initParsers = require('./init_parsers');
const initHeaders = require('./init_headers');
const initRoutes = require('./init_routes');
const initErrorHanlers = require('./init_error_handlers');

function init(app) {
  initParsers(app);
  initHeaders(app);
  initRoutes(app);
  initErrorHanlers(app);
}

module.exports = init;
