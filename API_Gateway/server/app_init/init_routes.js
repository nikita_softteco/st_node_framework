const mountPaths = require('../routes/mount_paths');
const authRouter = require('../routes/auth');
const pdfGeneratorRouter = require('../routes/pdf_generator');
const collectSpec = require('../utils/open_api/collect_spec');

function init(app) {
  app.use(mountPaths.auth, authRouter);
  app.use(mountPaths.pdfGenerator, pdfGeneratorRouter);
  app.get(mountPaths.spec, collectSpec);
}

module.exports = init;
