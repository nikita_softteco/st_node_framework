const bodyParser = require('body-parser');
const multer = require('multer');

const upload = multer();

function init(app) {
  app.use(bodyParser.json());
  app.use(upload.none());
}

module.exports = init;
