const validate = require('../utils/validation');
const schemas = require('../schemas/input_schemas');
const banks = require('../banks');
const bankModel = require('../db/queries/banks');
const { BadRequestError } = require('../errors');

function parseErrorMessage(msg) {
  const troubles = {};
  msg
    .match(/\[[A-Za-z0-9. "/-:]*\]/g)
    .forEach((trouble) => {
      const [field] = trouble.match(/"[A-Za-z]+"/g);
      const clearedField = field.replace(/"/g, '');
      const description = trouble.slice(4 + clearedField.length, -1);
      troubles[clearedField] = description;
    });
  return troubles;
}

const features = {};

features.check = async function check(body) {
  const request = validate(body, schemas.credit.post.body);
  const validated = {};
  const bankRequest = {};
  const checks = [];

  const checkFields = async function checkForBank(bankId) {
    const bank = await bankModel.findBank({ id: bankId });
    if (!bank) throw new BadRequestError(`Invalid bank id: ${bankId}`);
    const result = {
      status: 200,
    };
    try {
      bankRequest[bankId] = validate(request.fields, schemas.banks[bankId]);
    } catch (err) {
      result.status = err.status;
      result.fields = parseErrorMessage(err.message);
    }
    validated[bankId] = result;
  };

  request.banks.forEach((bankId) => {
    checks.push(checkFields(bankId));
  });

  await Promise.all(checks);
  return { email: request.email, validated, bankRequest };
};

features.sendToBanks = async function send({ email, bankRequest }) {
  // const requests = [];
  // Object.keys(bankRequest).forEach((bankId) => {
  //   requests.push(banks[bankId].sendRequest(bankRequest[bankId]));
  // });
  // Promise.all(requests)
  //   .then((responses) => {
  //     responses.forEach((res) => {
  //       sendEmail(email, res);
  //     });
  //   })
  //   .catch((err) => {
  //     throw err;
  //   });
};

module.exports = features;
