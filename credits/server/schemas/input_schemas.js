const joi = require('../utils/custom_joi');

const structure = require('./api_structure');

const { cloneStructure } = require('../lib/operations');

const schemas = cloneStructure(structure);

schemas.credit.post.body = joi.object().keys({
  email: joi.string().email().required(),
  banks: joi.array().items(joi.number().integer().min(0)).min(1).required(),
  fields: joi.object().required(),
});

schemas.banks = {
  12345: joi.object().keys({
    firstname: joi.string().min(5).max(15).required(),
    lastname: joi.string().min(5).max(15).required(),
    birthdate: joi.string().min(8).max(8).required(),
    salary: joi.number().integer().optional(),
  }),
  54321: joi.object().keys({
    firstname: joi.string().min(5).max(15).required(),
    lastname: joi.string().min(5).max(15).required(),
    age: joi.number().integer().min(18).required(),
    salary: joi.number().integer().required(),
  }),
  // 1: joi.object().keys({
  //   firstname: joi.string().min(5).max(15).required(),
  //   lastname: joi.string().min(5).max(15).required(),
  //   birthdate: joi.birthdate().splitted('/').optional(),
  // }),
};

module.exports = schemas;
