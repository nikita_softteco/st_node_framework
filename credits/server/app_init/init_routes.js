const router = require('../routes');

function init(app) {
  app.use(router);
}

module.exports = init;
