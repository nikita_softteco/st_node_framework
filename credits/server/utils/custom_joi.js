const joi = require('@hapi/joi');

const customJoi = joi.extend({
  base: joi.string(),
  name: 'birthdate',
  language: {
    splitted: 'needs to be splitted with {{spl}}',
  },
  rules: [{
    name: 'splitted',
    params: {
      spl: joi.string().max(1),
    },
    validate(params, value, state, options) {
      const parts = value.split(params.spl);
      if (parts.length !== 3) return this.createError('birthdate.splitted', { v: value, spl: params.spl }, state, options);
      return parts.join('-');
    },
  }],
});

module.exports = customJoi;
