const joi = require('./custom_joi');

const { BadRequestError } = require('../errors');

const defaultOptions = {
  convert: false,
  stripUnknown: true,
  abortEarly: false,
};

function validate(value, schema, options) {
  const validationOptions = {
    ...defaultOptions,
    ...options,
  };

  const validated = joi.validate(value, schema, validationOptions);

  if (validated.error) throw new BadRequestError(validated.error.message);
  return validated.value;
}

module.exports = validate;
