class HttpError extends Error {
  constructor({ status = 500, message = 'Something went wrong in service' }) {
    super(message);
    this.status = status;
  }
}

class BadRequestError extends HttpError {
  constructor(message = 'Data validation failed') {
    super({ status: 400, message });
    this.name = 'BadRequest';
  }
}

module.exports = {
  HttpError,
  BadRequestError,
};
