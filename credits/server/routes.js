const express = require('express');

const features = require('./features');

const router = express.Router();

router.post('/credit', async (req, res, next) => {
  let response;
  try {
    response = await features.fields.check(req.body);
  } catch (err) {
    return next(err);
  }
  res.json(response.validated);

  try {
    if (Object.keys(response.bankRequest).length) {
      await features.fields.sendToBanks(response);
    }
  } catch (err) {
    return next(err);
  }
});

module.exports = router;
