const mongoose = require('mongoose');

const bankSchema = new mongoose.Schema({
  id: Number,
  bankname: String,
  tag: String,
  creditUrl: String,
}, {
  versionKey: false,
});

module.exports = bankSchema;
