const mongoose = require('mongoose');

const bankModel = require('../models/banks');

const Bank = mongoose.model('Bank', bankModel);

const queries = {};

queries.findBank = async function find(selector) {
  return Bank.findOne(selector);
};

module.exports = queries;
