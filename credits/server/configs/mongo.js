const mongoose = require('mongoose');

// const { MONGO_HOST, NODE_ENV } = process.env;
const MONGO_HOST = 'localhost';

// if (NODE_ENV === 'test') {
//   mongoose.connect(`mongodb://${MONGO_HOST}/banks_test`, { useNewUrlParser: true });
// } else {
  mongoose.connect(`mongodb://${MONGO_HOST}/banks`, { useNewUrlParser: true });
// }
