const axios = require('axios');

const { HttpError } = require('../errors');

class BankInterface {
  constructor(url) {
    this.baseURL = url;
  }

  async sendRequest(options) {
    const reqOptions = {
      baseURL: this.baseURL,
      ...options,
    };

    let res;
    try {
      res = await axios(reqOptions);
    } catch (error) {
      if (error.response) {
        throw new HttpError({
          status: error.response.status,
          message: error.response.data.error.message,
        });
      }
      if (error.message) throw new HttpError({ message: error.message });
    }

    return {
      status: res.status,
      body: res.data,
    };
  }
}

module.exports = BankInterface;
