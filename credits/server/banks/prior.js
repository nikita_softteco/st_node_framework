const BankInterface = require('./interface');

const { PRIOR_CREDIT_URL } = process.env;

class Prior extends BankInterface {
  constructor() {
    super(PRIOR_CREDIT_URL);
  }
}

module.exports = Prior;
