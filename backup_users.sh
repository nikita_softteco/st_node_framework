#!/bin/sh

export CONTAINER_NAME="auth_mongo"
export DATABASE_NAME="users"
export BACKUP_LOCATION="/home/dell203/project/mongo-backups/users"

export TIMESTAMP=$(date +'%m%d%H%M')

docker exec -t ${CONTAINER_NAME} mongodump --out /data/${DATABASE_NAME}-backup-${TIMESTAMP} --db ${DATABASE_NAME}
docker cp ${CONTAINER_NAME}:/data/${DATABASE_NAME}-backup-${TIMESTAMP} ${BACKUP_LOCATION}